package com.gitlab.fritzy.back.app.local;

import org.springframework.boot.SpringApplication;
import com.gitlab.fritzy.back.FritzyBack;

/**
 * Run {@link FritzyBack} in the local environment.
 * 
 * @author Olivier LE JACQUES
 *
 */
public class AppWithEclipse {

  public static final String PROFILE_ECLIPSE = "eclipse";
  public static final String PROFILE_H2 = "h2";
  public static final String PROFILE_LOCAL = "local";


  public static void main(String[] args) {
    SpringApplication springApplication = new SpringApplication(FritzyBack.class);
    springApplication.setAdditionalProfiles(PROFILE_ECLIPSE, PROFILE_H2, PROFILE_LOCAL);
    springApplication.run(args);
  }

}

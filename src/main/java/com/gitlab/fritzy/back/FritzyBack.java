package com.gitlab.fritzy.back;

import java.util.TimeZone;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextRefreshedEvent;
import lombok.extern.slf4j.Slf4j;

/**
 * Fritzy back application.
 * 
 * @author Olivier LE JACQUES
 *
 */
@Slf4j
@SpringBootApplication(proxyBeanMethods = false)
public class FritzyBack implements ApplicationListener<ContextRefreshedEvent> {

  @Autowired
  private DataSourceProperties dataSourceProperties;

  /**
   * Creates the error properties used to setup the global REST error controller.
   * 
   * @return a new error properties
   */
  @Bean
  public ErrorProperties errorProperties() {
    return new ErrorProperties();
  }

  @Override
  public void onApplicationEvent(ContextRefreshedEvent event) {
    log.info("Started with datasource on url [{}]", dataSourceProperties.getUrl());
  }

  public static void main(String[] args) {
    SpringApplication.run(FritzyBack.class, args);
  }

  @PostConstruct
  void started() {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
  }
}
